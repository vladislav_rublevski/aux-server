package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.AuctionDto;
import by.bsuir.aux.repository.dto.ItemDto;
import by.bsuir.aux.repository.entity.Auction;
import by.bsuir.aux.repository.entity.Customer;
import by.bsuir.aux.repository.entity.Item;
import by.bsuir.aux.repository.entity.Seller;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class AuctionMapper{

    private ModelMapper modelMapper;
    private ItemMapper itemMapper;

    public AuctionMapper() {
        modelMapper = new ModelMapper();
        itemMapper = new ItemMapper();
        modelMapper.addMappings(new PropertyMap<Auction, AuctionDto>() {
            @Override
            protected void configure() {
                Converter<Set<Item>, Queue<ItemDto>> itemsConverter = new AbstractConverter<>() {
                    @Override
                    protected Queue<ItemDto> convert(Set<Item> items) {
                        Queue<ItemDto> result = new ArrayDeque<>(items.size());
                        items.stream()
                                .sorted(Comparator.comparingInt(Item::getOrder))
                                .forEach(item -> {
                                    ItemDto itemDto = itemMapper.mapToDto(item);
                                    result.add(itemDto);
                                });
                        return result;
                    }
                };
                using(itemsConverter).map(source.getItems(), destination.getItemDtoQueue());

                Converter<Set<Customer>, Set<Long>> customerConverter = new AbstractConverter<>() {
                    @Override
                    protected Set<Long> convert(Set<Customer> customers) {
                        return customers.parallelStream().map(Customer::getId).collect(Collectors.toSet());
                    }
                };
                using(customerConverter).map(source.getCustomers(), destination.getCustomersIds());
            }
        });
        modelMapper.addMappings(new PropertyMap<AuctionDto, Auction>() {
            @Override
            protected void configure() {
                Converter<Queue<ItemDto>, Set<Item>>  itemsConverter = new AbstractConverter<>() {
                    @Override
                    protected Set<Item> convert(Queue<ItemDto> itemDtos) {
                        Set<Item> result = new HashSet<>();
                        int order = 0;
                        for (ItemDto itemDto : itemDtos) {
                            Item item = itemMapper.mapToEntity(itemDto);
                            item.setOrder(order);
                            order++;
                            result.add(item);
                        }
                        return result;
                    }
                };
                using(itemsConverter).map(source.getItemDtoQueue(), destination.getItems());
            }
        });
    }

    public AuctionDto mapToDto(Auction auction){
        AuctionDto dto = modelMapper.map(auction, AuctionDto.class);
        Seller seller = auction.getSeller();
        dto.setSellerId(seller.getId());
        return dto;
    }

    public Auction mapToEntity(AuctionDto auctionDto, Seller seller, Set<Customer> customers) {
        Auction entity = modelMapper.map(auctionDto, Auction.class);
        entity.setSeller(seller);
        customers.forEach(entity::addCustomer);
        return entity;
    }
}
