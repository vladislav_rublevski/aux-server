package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.ItemDto;
import by.bsuir.aux.repository.entity.Item;
import org.modelmapper.ModelMapper;

public class ItemMapper {
    private ModelMapper modelMapper;

    public ItemMapper() {
        modelMapper = new ModelMapper();
    }

    public ItemDto mapToDto(Item item){
        return modelMapper.map(item, ItemDto.class);
    }

    public Item mapToEntity(ItemDto itemDto) {
        return modelMapper.map(itemDto, Item.class);
    }
}
