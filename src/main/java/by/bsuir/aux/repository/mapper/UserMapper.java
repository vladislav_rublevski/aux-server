package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.*;
import by.bsuir.aux.repository.entity.*;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import java.util.HashSet;
import java.util.Set;

public class UserMapper {

    private ModelMapper modelMapper;
    private AuctionMapper auctionMapper;

    public UserMapper() {
        modelMapper = new ModelMapper();
        auctionMapper = new AuctionMapper();
        modelMapper.addMappings(new PropertyMap<Seller, SellerDto>() {
            @Override
            protected void configure() {
                Converter<Set<Auction>, Set<AuctionDto>> auctionConverter = new AbstractConverter<>() {
                    @Override
                    protected Set<AuctionDto> convert(Set<Auction> auctions) {
                        Set<AuctionDto> result = new HashSet<>();
                        for (Auction auction : auctions) {
                            AuctionDto auctionDto = auctionMapper.mapToDto(auction);
                            result.add(auctionDto);
                        }
                        return result;
                    }
                };
                using(auctionConverter).map(source.getAuctions(), destination.getAuctions());
            }
        });

        modelMapper.addMappings(new PropertyMap<Customer, CustomerDto>() {
            @Override
            protected void configure() {
                Converter<Set<Auction>, Set<AuctionDto>> auctionConverter = new AbstractConverter<>() {
                    @Override
                    protected Set<AuctionDto> convert(Set<Auction> auctions) {
                        Set<AuctionDto> result = new HashSet<>();
                        for (Auction auction : auctions) {
                            AuctionDto auctionDto = auctionMapper.mapToDto(auction);
                            result.add(auctionDto);
                        }
                        return result;
                    }
                };
                using(auctionConverter).map(source.getAuctions(), destination.getAuctions());
            }
        });

        modelMapper.addMappings(new PropertyMap<UserDto, User>() {
            @Override
            protected void configure() {
                Converter<CustomerDto, Customer> customerConverter = new AbstractConverter<>() {
                    @Override
                    protected Customer convert(CustomerDto customerDto) {
                        return null;
                    }
                };
                using(customerConverter).map(source.getCustomer(), destination.getCustomer());

                Converter<PersonalInfoDto, PersonalInfo> personalInfoConverter = new AbstractConverter<>() {
                    @Override
                    protected PersonalInfo convert(PersonalInfoDto personalInfoDto) {
                        return null;
                    }
                };
                using(personalInfoConverter).map(source.getPersonalInfo(), destination.getPersonalInfo());

                Converter<SellerDto, Seller> sellerConverter = new AbstractConverter<>() {
                    @Override
                    protected Seller convert(SellerDto sellerDto) {
                        return null;
                    }
                };
                using(sellerConverter).map(source.getSeller(), destination.getSeller());
            }
        });

    }

    public UserDto mapToDto(User user){
        return modelMapper.map(user, UserDto.class);
    }

    public User mapToEntity(UserDto userDto, PersonalInfo personalInfo, Customer customer, Seller seller){
        User user = modelMapper.map(userDto, User.class);
        user.setPersonalInfo(personalInfo);
        user.setCustomer(customer);
        user.setSeller(seller);
        return user;
    }
}
