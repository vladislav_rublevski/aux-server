package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.AuctionDto;
import by.bsuir.aux.repository.dto.SellerDto;
import by.bsuir.aux.repository.entity.Auction;
import by.bsuir.aux.repository.entity.Seller;
import by.bsuir.aux.repository.entity.User;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import java.util.HashSet;
import java.util.Set;

public class SellerMapper {
    private ModelMapper modelMapper;
    private AuctionMapper auctionMapper;

    public SellerMapper() {
        modelMapper = new ModelMapper();
        auctionMapper = new AuctionMapper();
        modelMapper.addMappings(new PropertyMap<Seller, SellerDto>() {
            @Override
            protected void configure() {
                Converter<Set<Auction>, Set<AuctionDto>> auctionConverter = new AbstractConverter<>() {
                    @Override
                    protected Set<AuctionDto> convert(Set<Auction> auctions) {
                        Set<AuctionDto> result = new HashSet<>();
                        for (Auction auction : auctions) {
                            AuctionDto auctionDto = auctionMapper.mapToDto(auction);
                            result.add(auctionDto);
                        }
                        return result;
                    }
                };
                using(auctionConverter).map(source.getAuctions(), destination.getAuctions());

                Converter<User, Long> userConverter = new AbstractConverter<>() {
                    @Override
                    protected Long convert(User user) {
                        return user.getId();
                    }
                };
                using(userConverter).map(source.getUser(), destination.getUserId());
            }
        });
        modelMapper.addMappings(new PropertyMap<SellerDto, Seller>() {
            @Override
            protected void configure() {
                Converter<Long, User> userConverter = new AbstractConverter<>() {
                    @Override
                    protected User convert(Long aLong) {
                        return null;
                    }
                };
                using(userConverter).map(source.getUserId(), destination.getUser());
            }
        });
    }

    public SellerDto mapToDto(Seller seller){
        return modelMapper.map(seller, SellerDto.class);
    }

    public Seller mapToEntity(SellerDto sellerDto, User user, Set<Auction> auctions){
        Seller seller = modelMapper.map(sellerDto, Seller.class);
        seller.setUser(user);
        auctions.forEach(seller::addAuction);
        return seller;
    }
}
