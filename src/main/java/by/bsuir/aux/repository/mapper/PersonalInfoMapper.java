package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.PersonalInfoDto;
import by.bsuir.aux.repository.entity.PersonalInfo;
import org.modelmapper.ModelMapper;

public class PersonalInfoMapper {

    private ModelMapper modelMapper;

    public PersonalInfoMapper() {
        this.modelMapper = new ModelMapper();
    }

    public PersonalInfoDto mapToDto(PersonalInfo personalInfo){
        return modelMapper.map(personalInfo, PersonalInfoDto.class);
    }

    public PersonalInfo mapToEntity(PersonalInfoDto dto) {
        return modelMapper.map(dto, PersonalInfo.class);
    }
}
