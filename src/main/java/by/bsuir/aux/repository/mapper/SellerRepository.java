package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.entity.Seller;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SellerRepository  extends JpaRepository<Seller, Long> {
}
