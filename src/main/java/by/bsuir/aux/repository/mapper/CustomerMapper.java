package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.AuctionDto;
import by.bsuir.aux.repository.dto.CustomerDto;
import by.bsuir.aux.repository.entity.Auction;
import by.bsuir.aux.repository.entity.Customer;
import by.bsuir.aux.repository.entity.User;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import java.util.HashSet;
import java.util.Set;

public class CustomerMapper {
    private ModelMapper modelMapper;
    private AuctionMapper auctionMapper;


    public CustomerMapper() {
        modelMapper = new ModelMapper();
        auctionMapper = new AuctionMapper();

        modelMapper.addMappings(new PropertyMap<Customer, CustomerDto>() {
            @Override
            protected void configure() {
                Converter<Set<Auction>, Set<AuctionDto>> auctionConverter = new AbstractConverter<>() {
                    @Override
                    protected Set<AuctionDto> convert(Set<Auction> auctions) {
                        Set<AuctionDto> result = new HashSet<>();
                        for (Auction auction : auctions) {
                            AuctionDto auctionDto = auctionMapper.mapToDto(auction);
                            result.add(auctionDto);
                        }
                        return result;
                    }
                };
                using(auctionConverter).map(source.getAuctions(), destination.getAuctions());

                Converter<User, Long> userConverter = new AbstractConverter<>() {
                    @Override
                    protected Long convert(User user) {
                        return user.getId();
                    }
                };
                using(userConverter).map(source.getUser(), destination.getId());
            }
        });
        modelMapper.addMappings(new PropertyMap<CustomerDto, Customer>() {
            @Override
            protected void configure() {
                Converter<Long, User> userConverter = new AbstractConverter<>() {
                    @Override
                    protected User convert(Long aLong) {
                        return null;
                    }
                };
                using(userConverter).map(source.getUserId(), destination.getUser());
            }
        });
    }

    public CustomerDto mapToDto(Customer customer){
        return modelMapper.map(customer, CustomerDto.class);
    }

    public Customer mapToEntity(CustomerDto customerDto, User user, Set<Auction> auctions){
        Customer customer = modelMapper.map(customerDto, Customer.class);
        customer.setUser(user);
        auctions.forEach(customer::addAuction);
        return customer;
    }
}
