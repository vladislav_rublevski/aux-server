package by.bsuir.aux.repository.repository;

import by.bsuir.aux.repository.entity.Auction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuctionRepository extends JpaRepository<Auction, Long> {
}
