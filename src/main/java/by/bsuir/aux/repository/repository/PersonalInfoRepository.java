package by.bsuir.aux.repository.repository;

import by.bsuir.aux.repository.entity.PersonalInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonalInfoRepository extends JpaRepository<PersonalInfo, Long> {

}
