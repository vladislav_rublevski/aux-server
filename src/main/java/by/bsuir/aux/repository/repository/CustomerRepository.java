package by.bsuir.aux.repository.repository;

import by.bsuir.aux.repository.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
