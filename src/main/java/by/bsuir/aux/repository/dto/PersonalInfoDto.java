package by.bsuir.aux.repository.dto;

/**
 * A leaf of the Dto Tree
 */
public class PersonalInfoDto {
    private String name;

    public PersonalInfoDto() {
    }

    public PersonalInfoDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
