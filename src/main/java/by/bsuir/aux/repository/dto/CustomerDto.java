package by.bsuir.aux.repository.dto;

import java.util.HashSet;
import java.util.Set;

public class CustomerDto {
    private long id;
    private int rate;
    private Set<AuctionDto> auctions;

    public CustomerDto() {
        auctions = new HashSet<>();
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public Set<AuctionDto> getAuctions() {
        return auctions;
    }

    public void setAuctions(Set<AuctionDto> auctions) {
        this.auctions = auctions;
    }

}
