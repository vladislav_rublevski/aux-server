package by.bsuir.aux.repository.dto;

public class BidDto {
    private long auctionId;
    private double newPrice;

    public BidDto() {
    }

    public BidDto(long auctionId, double newPrice) {
        this.auctionId = auctionId;
        this.newPrice = newPrice;
    }

    public long getAuctionId() {
        return auctionId;
    }

    public double getNewPrice() {
        return newPrice;
    }
}
