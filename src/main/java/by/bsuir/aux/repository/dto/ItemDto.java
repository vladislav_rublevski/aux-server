package by.bsuir.aux.repository.dto;

/**
 * A leaf of the Dto Tree
 */
public class ItemDto {
    private String name;
    private String description;
    private int rate;
    private int currentPrice;

    public ItemDto() {
    }

    public ItemDto(String name, String description, int rate, int currentPrice) {
        this.name = name;
        this.description = description;
        this.rate = rate;
        this.currentPrice = currentPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(int currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
