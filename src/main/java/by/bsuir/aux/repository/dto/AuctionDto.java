package by.bsuir.aux.repository.dto;


import by.bsuir.aux.repository.mapper.LocalDateDeserializer;
import by.bsuir.aux.repository.mapper.LocalDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayDeque;
import java.util.Queue;

public class AuctionDto implements Serializable {
    private long id;
    private Queue<ItemDto> itemDtoQueue;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate beginningDate;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate expirationDate;

    private long sellerId;

    public AuctionDto() {
        itemDtoQueue = new ArrayDeque<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void addToQueue(ItemDto itemDto){
        itemDtoQueue.add(itemDto);
    }

    public ItemDto getFirstItem() {
        return itemDtoQueue.peek();
    }

    public ItemDto removeFirstItem(){
        return itemDtoQueue.remove();
    }

    public LocalDate getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(LocalDate beginningDate) {
        this.beginningDate = beginningDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public long getSellerId() {
        return sellerId;
    }

    public void setSellerId(long sellerId) {
        this.sellerId = sellerId;
    }

    public Queue<ItemDto> getItemDtoQueue() {
        return itemDtoQueue;
    }

    public void setItemDtoQueue(Queue<ItemDto> itemDtoQueue) {
        this.itemDtoQueue = itemDtoQueue;
    }
}
