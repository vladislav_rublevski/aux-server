package by.bsuir.aux.repository.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * May not have an equal() method provided that detached entities of this class will never be stored in Set, etc.
 */
@Entity
@Table(name = "auction", schema = "public")
public class Auction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "beginning_date")
    private LocalDate beginningDate;
    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "seller_id")
    private Seller seller;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "customer_auction",
            joinColumns = @JoinColumn(name = "cust_id"),
            inverseJoinColumns = @JoinColumn(name = "aux_id")
    )
    private Set<Customer> customers = new HashSet<>();

    @OneToMany(mappedBy = "auction")
    private Set<Item> items = new HashSet<>();

    public Auction() {
    }

    public long getId() {
        return id;
    }

    public LocalDate getBeginningDate() {
        return beginningDate;
    }

    public void setBeginningDate(LocalDate beginningDate) {
        this.beginningDate = beginningDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        seller.addAuction(this);
        this.seller = seller;
    }

    public void addCustomer(Customer customer){
        customers.add(customer);
        customer.addAuction(this);
    }

    public void removeCustomer(Customer customer){
        customers.remove(customer);
        customer.removeAuction(this);
    }

    public Set<Item> getItems() {
        return items;
    }

    public Set<Customer> getCustomers() {
        return customers;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public void addItem(Item item){
        item.setAuction(this);
        this.items.add(item);
    }

    public void removeItem(Item item){
        this.items.remove(item);
        item.setAuction(null);
    }

}
