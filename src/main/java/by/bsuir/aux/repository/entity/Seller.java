package by.bsuir.aux.repository.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * May not have an equal() method provided that detached entities of this class will never be stored in Set, etc.
 */
@Entity
public class Seller {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int rate;

    public Seller(long id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "seller")
    private Set<Auction> auctions = new HashSet<>();

    @OneToOne(mappedBy = "seller")
    private User user;


    public Seller() {
        auctions = new HashSet<>();
    }

    public long getId() {
        return id;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void addAuction(Auction auction){
        auctions.add(auction);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    /**
     * For mapping purposes only
     */
    public void setId(long id) {
        this.id = id;
    }

    public Set<Auction> getAuctions() {
        return auctions;
    }
}
