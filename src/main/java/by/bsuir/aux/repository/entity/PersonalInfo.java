package by.bsuir.aux.repository.entity;

import javax.persistence.*;

@Entity
@Table(name = "personal_info", schema = "public")
public class PersonalInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    public PersonalInfo() {
    }

    /**
     * For test purposes only
     */
    public PersonalInfo(long id){
        this.id = id;
    }

    @OneToOne(mappedBy = "personalInfo")
    private User user;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getId() {
        return id;
    }
}
