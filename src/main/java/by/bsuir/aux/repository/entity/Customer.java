package by.bsuir.aux.repository.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table( name = "customer", schema = "public")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int rate;

    @ManyToMany(mappedBy = "customers")
    private Set<Auction> auctions = new HashSet<>();

    @OneToOne(mappedBy = "customer")
    private User user;

    /**
     * Items where this customer offered the largest price
     */
    @OneToMany(mappedBy = "customer")
    private Set<Item> items;

    public Customer() {
        auctions = new HashSet<>();
    }

    public Customer(long id) {
        this.id = id;
    }

    public Set<Auction> getAuctions() {
        return auctions;
    }

    public long getId() {
        return id;
    }

    /**
     *For mappings purpose only
     */
    public void setId(long id) {
        this.id = id;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void addAuction(Auction auction){
        auctions.add(auction);
    }

    public void removeAuction(Auction auction){
        auctions.remove(auction);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
