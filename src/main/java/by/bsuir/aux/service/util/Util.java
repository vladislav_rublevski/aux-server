package by.bsuir.aux.service.util;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Util {

    public static <T> Optional<Set<T>> setOfOptionalsToOptionalSet(Set<Optional<T>> setOfOptionals){
        boolean allPresent = setOfOptionals.parallelStream().allMatch(Optional::isPresent);
        if (allPresent){
            return Optional.of(setOfOptionals.parallelStream()
                    .map(Optional::get)
                    .collect(Collectors.toSet()));
        }else{
            return Optional.empty();
        }
    }
}
