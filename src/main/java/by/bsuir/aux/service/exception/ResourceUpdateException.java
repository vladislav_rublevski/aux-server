package by.bsuir.aux.service.exception;

public class ResourceUpdateException extends RuntimeException {
    private static final String ERROR_PATTERN = "Unable to update resource %s with id %d";

    public ResourceUpdateException(String resourceName, long id) {
        super(String.format(ERROR_PATTERN, resourceName, id));
    }
}
