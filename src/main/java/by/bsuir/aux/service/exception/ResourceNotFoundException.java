package by.bsuir.aux.service.exception;

public class ResourceNotFoundException extends RuntimeException {
    private static final String PATTERN_ERROR_ID = "Resource of class %s with id %d not found";
    private static final String PATTERN_ERROR_GENERAL = "Resource of class %s with id %d not found";

    public ResourceNotFoundException(String resourceName, long id){
        super(String.format(PATTERN_ERROR_ID, resourceName, id));
    }

    public ResourceNotFoundException(String resourceName){
        super(String.format(PATTERN_ERROR_GENERAL, resourceName));
    }
}
