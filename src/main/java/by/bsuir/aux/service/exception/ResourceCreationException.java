package by.bsuir.aux.service.exception;

public class ResourceCreationException extends RuntimeException {
    private static final String ERROR_PATTERN = "Unable to create %s";


    public ResourceCreationException(String resourceName){
        super(String.format(ERROR_PATTERN, resourceName));
    }
}
