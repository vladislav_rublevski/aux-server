package by.bsuir.aux.service.logic.merge;

public interface Merger {
    /*
    * Guaranteed that all child nodes and all nodes of children in aNew object are present in old.
    * */
    void merge(Object old, Object aNew);
}
