package by.bsuir.aux.service.logic.merge;

import by.bsuir.aux.repository.dto.ItemDto;
import by.bsuir.aux.repository.entity.Item;

public class ItemMerger {

    /**
     *Updates should be reflected in oldItem. The method is executed in jpa transaction.
     */
    public static void merge(Item old, ItemDto aNew) {
        throw new UnsupportedOperationException();
    }
}
