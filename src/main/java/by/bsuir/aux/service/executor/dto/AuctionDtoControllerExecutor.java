package by.bsuir.aux.service.executor.dto;

import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.AuctionDto;
import by.bsuir.aux.service.logic.merge.AuctionMerger;

import java.util.function.BiFunction;

public class AuctionDtoControllerExecutor extends AbstractDtoControllerExecutor<AuctionDto> {

    @Override
    Target getTargetMapper() {
        return Target.AUCTION_MAPPER;
    }

    @Override
    String getResourceName() {
        return AuctionDto.class.getName();
    }

    @Override
    BiFunction<AuctionDto, AuctionDto, AuctionDto> getMerger() {
        return (old, aNew) -> AuctionMerger.merge(old, aNew);
    }
}
