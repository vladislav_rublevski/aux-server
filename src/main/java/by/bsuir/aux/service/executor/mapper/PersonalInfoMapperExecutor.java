package by.bsuir.aux.service.executor.mapper;

import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.PersonalInfoDto;
import by.bsuir.aux.repository.entity.PersonalInfo;
import by.bsuir.aux.repository.mapper.PersonalInfoMapper;
import by.bsuir.aux.service.logic.merge.PersonalInfoMerger;
import by.bsuir.aux.service.logic.sync.PersonalInfoSynchronizer;

public class PersonalInfoMapperExecutor extends AbstractMapperExecutor<PersonalInfo, PersonalInfoDto> {

    private PersonalInfoMapper personalInfoMapper;

    public PersonalInfoMapperExecutor() {
        personalInfoMapper = new PersonalInfoMapper();
    }

    @Override
    Target getEntityControllerTarget() {
        return Target.PERSONAL_INFO_CONTROLLER;
    }

    @Override
    PersonalInfoDto mapToDto(PersonalInfo entity) {
        return personalInfoMapper.mapToDto(entity);
    }

    @Override
    PersonalInfo buildExampleFromDto(PersonalInfoDto dto) {
        return PersonalInfoSynchronizer.exampleOf(dto);
    }


    @Override
    String getEntityName() {
        return PersonalInfo.class.getName();
    }

    @Override
    void merge(PersonalInfo oldEntity, PersonalInfoDto newDto) {
        PersonalInfoMerger.merge(oldEntity, newDto);
    }

    @Override
    PersonalInfo buildIdExampleFrom(PersonalInfoDto newDto, long id) {
        /*
         * PersonalInfoDto does not have an id
         */
        throw new UnsupportedOperationException();
    }
}
