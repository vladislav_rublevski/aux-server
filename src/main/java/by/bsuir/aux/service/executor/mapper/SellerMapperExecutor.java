package by.bsuir.aux.service.executor.mapper;

import by.bsuir.aux.controller.command.AuctionCommand;
import by.bsuir.aux.controller.command.CommandType;
import by.bsuir.aux.controller.command.ControllerCommand;
import by.bsuir.aux.controller.proxy.Proxy;
import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.AuctionDto;
import by.bsuir.aux.repository.dto.SellerDto;
import by.bsuir.aux.repository.entity.Auction;
import by.bsuir.aux.repository.entity.Seller;
import by.bsuir.aux.repository.mapper.SellerMapper;
import by.bsuir.aux.service.exception.ResourceCreationException;
import by.bsuir.aux.service.exception.ResourceUpdateException;
import by.bsuir.aux.service.logic.merge.SellerMerger;
import by.bsuir.aux.service.logic.sync.AuctionSynchronizer;
import by.bsuir.aux.service.logic.sync.SellerSynchronizer;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class SellerMapperExecutor extends AbstractMapperExecutor<Seller, SellerDto> {

    private SellerMapper sellerMapper;

    public SellerMapperExecutor() {
        sellerMapper = new SellerMapper();
    }

    @Override
    Target getEntityControllerTarget() {
        return Target.SELLER_CONTROLLER;
    }

    @Override
    SellerDto mapToDto(Seller entity) {
        return sellerMapper.mapToDto(entity);
    }

    @Override
    Seller buildExampleFromDto(SellerDto dto) {
        return SellerSynchronizer.exampleOf(dto);
    }

    @Override
    String getEntityName() {
        return Seller.class.getName();
    }

    /**
     * Copy of CustomerMapperExecutor's merge
     */
    @Override
    void merge(Seller oldEntity, SellerDto newDto) {
        Set<AuctionDto> missingNodes = getMissingNodesOf(oldEntity, newDto);
        addMissingNodesTo(oldEntity, missingNodes);
        SellerMerger.merge(oldEntity, newDto);
    }

    @Override
    Seller buildIdExampleFrom(SellerDto newDto, long id) {
        newDto.setId(id);
        return SellerSynchronizer.idExampleOf(newDto);
    }

    private void addMissingNodesTo(Seller oldEntity, Set<AuctionDto> missingNodes) {
        for (AuctionDto dto : missingNodes){
            Optional<Auction> optionalFoundAuction = findByDtoExample(dto);
            if (optionalFoundAuction.isPresent()){
                Auction foundAuction = optionalFoundAuction.get();
                oldEntity.addAuction(foundAuction);
                continue;
            }
            Optional<Auction> optionalAddedAuction = addMissingAuctionFrom(dto);
            if (optionalAddedAuction.isPresent()) {
                Auction addedAuction = optionalAddedAuction.get();
                oldEntity.addAuction(addedAuction);
            }else {
                throw new ResourceCreationException(Auction.class.getName());
            }
        }
    }

    private Optional<Auction> addMissingAuctionFrom(AuctionDto dto) {
        AuctionCommand entityCreateCommand = new AuctionCommand(CommandType.CREATE);
        Auction example = AuctionSynchronizer.exampleOf(dto);
        entityCreateCommand.setModel(example);
        Proxy proxy = getProxy();
        proxy.dispatch(entityCreateCommand, Target.AUCTION_CONTROLLER);
        if (entityCreateCommand.getOptionalResult().isEmpty()){
            return Optional.empty();
        }

        Auction createdAuction = entityCreateCommand.getOptionalResult().get();
        ControllerCommand<AuctionDto> auctionUpdateCommand = new ControllerCommand<>(CommandType.UPDATE);
        long id = createdAuction.getId();
        dto.setId(id);
        auctionUpdateCommand.setId(id);
        auctionUpdateCommand.setModel(dto);
        proxy.dispatch(auctionUpdateCommand, Target.AUCTION_MAPPER);

        if (auctionUpdateCommand.getOptionalResult().isEmpty()){
            throw new ResourceUpdateException(Auction.class.getName(), id);
        }

        AuctionCommand auctionReadCommand = new AuctionCommand(CommandType.READ);
        Auction idExample = AuctionSynchronizer.idExampleOf(dto);
        auctionReadCommand.setModel(idExample);
        proxy.dispatch(auctionReadCommand, Target.AUCTION_CONTROLLER);
        return auctionReadCommand.getOptionalResult();
    }

    private Optional<Auction> findByDtoExample(AuctionDto dto) {
        Auction example = AuctionSynchronizer.idExampleOf(dto);
        AuctionCommand entityReadCommand = new AuctionCommand(CommandType.READ);
        entityReadCommand.setModel(example);
        Proxy proxy = getProxy();
        proxy.dispatch(entityReadCommand, Target.AUCTION_CONTROLLER);
        if (entityReadCommand.getOptionalResults().isPresent()){
            Auction fst = entityReadCommand.getOptionalResults().get().get(0);
            return Optional.of(fst);
        }else {
            return Optional.empty();
        }
    }

    private Set<AuctionDto> getMissingNodesOf(Seller oldEntity, SellerDto newDto) {
        Set<AuctionDto> auctionDtos = newDto.getAuctions();
        Set<Auction> auctions = oldEntity.getAuctions();
        return auctionDtos.parallelStream().filter(dto -> {
            long dtoId = dto.getId();
            return auctions.parallelStream()
                    .map(Auction::getId)
                    .anyMatch((entityId -> entityId == dtoId));
        }).collect(Collectors.toUnmodifiableSet());
    }
}
