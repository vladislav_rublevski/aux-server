package by.bsuir.aux.service.executor.mapper;

import by.bsuir.aux.controller.command.Command;
import by.bsuir.aux.controller.command.CommandType;
import by.bsuir.aux.controller.command.ControllerCommand;
import by.bsuir.aux.controller.proxy.Proxy;
import by.bsuir.aux.controller.proxy.ProxyExecutor;
import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.service.exception.ResourceNotFoundException;

import java.util.Optional;

public abstract class AbstractMapperExecutor<E, D> extends ProxyExecutor<D> {

    abstract Target getEntityControllerTarget();
    abstract D mapToDto(E entity);
    abstract E buildExampleFromDto(D dto);
    abstract String getEntityName();
    abstract void merge(E oldEntity, D newDto);

    @Override
    public void run(Command<D> command) {
        ControllerCommand<D> controllerCommand = (ControllerCommand<D>) command;
        CommandType commandType = controllerCommand.getCommandType();
        switch (commandType){
            case READ:
                runReadCommand(controllerCommand);
                break;
            case CREATE:
                runCreateCommand(controllerCommand);
                break;
            case DELETE:
                runDeleteCommand(controllerCommand);
                break;
            case UPDATE:
                runUpdateCommand(controllerCommand);
                break;

        }
    }

    /*
     * Updates tree with existing root
     * */
    private void runUpdateCommand(ControllerCommand<D> dtoCommand) {
        D newDto = dtoCommand.getModel();
        long id = dtoCommand.getId();
        ControllerCommand<E> entityFindCommand = new ControllerCommand<>(CommandType.READ);
        E idExample = buildIdExampleFrom(newDto, id);
        entityFindCommand.setModel(idExample);
        Optional<E> optionalEntity = findModelById(entityFindCommand, getEntityControllerTarget());
        if (optionalEntity.isEmpty()){
            throw new ResourceNotFoundException(getEntityName(), id);
        }
        E oldEntity = optionalEntity.get();
        merge(oldEntity, newDto);
        ControllerCommand<E> updateEntityCommand = new ControllerCommand<>(CommandType.UPDATE);
        updateEntityCommand.setModel(oldEntity);
        updateEntityCommand.setId(id);
        Proxy proxy = getProxy();
        proxy.dispatch(updateEntityCommand, getEntityControllerTarget());
        convertResultTo(dtoCommand, updateEntityCommand);
    }

    abstract E buildIdExampleFrom(D newDto, long id);

    /**
     *Deletes tree with existing root and nodes
     */
    private void runDeleteCommand(ControllerCommand<D> dtoCommand) {
        long dtoId = dtoCommand.getId();
        ControllerCommand<E> entityCommand = new ControllerCommand<>(CommandType.DELETE);
        entityCommand.setId(dtoId);
        Proxy proxy = getProxy();
        proxy.dispatch(entityCommand, getEntityControllerTarget());
    }

    /*
    *Creates an Entity root of specified Dto from example
     */
    private void runCreateCommand(ControllerCommand<D> dtoCommand) {
        D dto = dtoCommand.getModel();
        E entity = buildExampleFromDto(dto);
        ControllerCommand<E> entityCommand = new ControllerCommand<>(CommandType.CREATE);
        entityCommand.setModel(entity);
        Proxy proxy = getProxy();
        proxy.dispatch(entityCommand, getEntityControllerTarget());
        convertResultTo(dtoCommand, entityCommand);
    }


    private <T> Optional<T> findModelById(Command<T> command, Target target) {
        Proxy proxy = getProxy();
        proxy.dispatch(command, target);
        if (command.getOptionalResults().isPresent()){
            T fst = command.getOptionalResults().get().get(0);
            return Optional.of(fst);
        }else{
            return Optional.empty();
        }
    }

    /**
     * Read existing tree with specified root
     */
    private void runReadCommand(ControllerCommand<D> dtoCommand) {
        long dtoId = dtoCommand.getId();
        ControllerCommand<E> entityCommand = new ControllerCommand<>(CommandType.READ);
        entityCommand.setId(dtoId);
        Proxy proxy = getProxy();
        proxy.dispatch(entityCommand, getEntityControllerTarget());
        convertResultTo(dtoCommand, entityCommand);
    }

    private void convertResultTo(ControllerCommand<D> dtoCommand, ControllerCommand<E> entityCommand) {
        if (entityCommand.getOptionalResult().isPresent()){
            E entity = entityCommand.getOptionalResult().get();
            D dto = mapToDto(entity);
            dtoCommand.setOptionalResult(Optional.of(dto));
        }else{
            dtoCommand.setOptionalResult(Optional.empty());
        }
    }

}
