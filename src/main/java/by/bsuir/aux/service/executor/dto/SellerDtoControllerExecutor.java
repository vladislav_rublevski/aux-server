package by.bsuir.aux.service.executor.dto;

import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.SellerDto;
import by.bsuir.aux.service.logic.merge.SellerMerger;

import java.util.function.BiFunction;

public class SellerDtoControllerExecutor extends AbstractDtoControllerExecutor<SellerDto> {
    @Override
    Target getTargetMapper() {
        return Target.SELLER_MAPPER;
    }

    @Override
    String getResourceName() {
        return SellerDto.class.getName();
    }

    @Override
    BiFunction<SellerDto, SellerDto, SellerDto> getMerger() {
        return (old, aNew) -> SellerMerger.merge(old, aNew);
    }
}
