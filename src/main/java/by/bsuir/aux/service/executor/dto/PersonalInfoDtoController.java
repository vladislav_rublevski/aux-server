package by.bsuir.aux.service.executor.dto;

import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.PersonalInfoDto;
import by.bsuir.aux.service.logic.merge.PersonalInfoMerger;

import java.util.function.BiFunction;

public class PersonalInfoDtoController extends AbstractDtoControllerExecutor<PersonalInfoDto> {
    @Override
    Target getTargetMapper() {
        return Target.PERSONAL_INFO_MAPPER;
    }

    @Override
    String getResourceName() {
        return PersonalInfoDto.class.getName();
    }

    @Override
    BiFunction<PersonalInfoDto, PersonalInfoDto, PersonalInfoDto> getMerger() {
        return PersonalInfoMerger::merge;
    }
}
