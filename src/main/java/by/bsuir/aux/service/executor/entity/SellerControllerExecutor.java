package by.bsuir.aux.service.executor.entity;

import by.bsuir.aux.repository.entity.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public class SellerControllerExecutor extends AbstractControllerExecutor<Seller> {

    @Autowired
    public SellerControllerExecutor(JpaRepository<Seller, Long> repository) {
        super(repository);
    }

}
