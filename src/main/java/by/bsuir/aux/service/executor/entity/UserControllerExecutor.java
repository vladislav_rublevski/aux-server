package by.bsuir.aux.service.executor.entity;

import by.bsuir.aux.repository.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public class UserControllerExecutor extends AbstractControllerExecutor<User> {

    public UserControllerExecutor(JpaRepository<User, Long> repository) {
        super(repository);
    }

}
