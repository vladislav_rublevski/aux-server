package by.bsuir.aux.service.executor.mapper;

import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.ItemDto;
import by.bsuir.aux.repository.entity.Item;
import by.bsuir.aux.repository.mapper.ItemMapper;
import by.bsuir.aux.service.logic.merge.ItemMerger;
import by.bsuir.aux.service.logic.sync.ItemSynchronizer;

public class ItemMapperExecutor extends AbstractMapperExecutor<Item, ItemDto> {
    private ItemMapper itemMapper;

    public ItemMapperExecutor() {
        itemMapper = new ItemMapper();
    }

    @Override
    Target getEntityControllerTarget() {
        return Target.ITEM_CONTROLLER;
    }

    @Override
    ItemDto mapToDto(Item entity) {
        return itemMapper.mapToDto(entity);
    }

    @Override
    Item buildExampleFromDto(ItemDto dto) {
        return ItemSynchronizer.exampleOf(dto);
    }

    @Override
    String getEntityName() {
        return Item.class.getName();
    }

    @Override
    void merge(Item oldEntity, ItemDto newDto) {
        ItemMerger.merge(oldEntity, newDto);
    }

    @Override
    Item buildIdExampleFrom(ItemDto newDto, long id) {
        /*
         * ItemDto does not have an id
         */
        throw new UnsupportedOperationException();
    }
}
