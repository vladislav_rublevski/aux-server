package by.bsuir.aux.service.executor.entity;

import by.bsuir.aux.repository.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public class CustomerControllerExecutor extends AbstractControllerExecutor<Customer> {

    @Autowired
    public CustomerControllerExecutor(JpaRepository<Customer, Long> repository) {
        super(repository);
    }

}
