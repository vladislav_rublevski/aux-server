package by.bsuir.aux.service.executor.mapper;

import by.bsuir.aux.controller.command.*;
import by.bsuir.aux.controller.proxy.Proxy;
import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.CustomerDto;
import by.bsuir.aux.repository.dto.PersonalInfoDto;
import by.bsuir.aux.repository.dto.SellerDto;
import by.bsuir.aux.repository.dto.UserDto;
import by.bsuir.aux.repository.entity.Customer;
import by.bsuir.aux.repository.entity.PersonalInfo;
import by.bsuir.aux.repository.entity.Seller;
import by.bsuir.aux.repository.entity.User;
import by.bsuir.aux.repository.mapper.UserMapper;
import by.bsuir.aux.service.exception.ResourceCreationException;
import by.bsuir.aux.service.exception.ResourceNotFoundException;
import by.bsuir.aux.service.exception.ResourceUpdateException;
import by.bsuir.aux.service.logic.merge.UserMerger;
import by.bsuir.aux.service.logic.sync.CustomerSynchronizer;
import by.bsuir.aux.service.logic.sync.PersonalInfoSynchronizer;
import by.bsuir.aux.service.logic.sync.SellerSynchronizer;
import by.bsuir.aux.service.logic.sync.UserSynchronizer;

import java.util.Optional;

public class UserMapperExecutor extends AbstractMapperExecutor<User, UserDto> {
    private UserMapper userMapper;

    public UserMapperExecutor() {
        userMapper = new UserMapper();
    }

    @Override
    Target getEntityControllerTarget() {
        return Target.USER_CONTROLLER;
    }

    @Override
    UserDto mapToDto(User entity) {
        return userMapper.mapToDto(entity);
    }

    @Override
    User buildExampleFromDto(UserDto dto) {
        return UserSynchronizer.exampleOf(dto);
    }

    @Override
    String getEntityName() {
        return User.class.getName();
    }

    @Override
    void merge(User oldEntity, UserDto newDto) {
        if (isCustomerMissing(oldEntity)){
            CustomerDto customerDto = newDto.getCustomer();
            addCustomerTo(oldEntity, customerDto);
        }

        if (isSellerMissing(oldEntity)){
            SellerDto sellerDto = newDto.getSeller();
            addSellerTo(oldEntity, sellerDto);
        }

        if (isPersonalInfoMissing(oldEntity)){
            PersonalInfoDto personalInfoDto = newDto.getPersonalInfo();
            addPersonalInfoTo(oldEntity, personalInfoDto);
        }

        UserMerger.merge(oldEntity, newDto);
    }

    @Override
    User buildIdExampleFrom(UserDto newDto, long id) {
        newDto.setId(id);
        return UserSynchronizer.idExampleOf(newDto);
    }

    private void addPersonalInfoTo(User oldEntity, PersonalInfoDto personalInfoDto) {
        /*
          A leaf of the Dto Tree.So no updates.
         */
        PersonalInfo example = PersonalInfoSynchronizer.exampleOf(personalInfoDto);
        ControllerCommand<PersonalInfo> createEntityCommand = new ControllerCommand<>(CommandType.CREATE);
        createEntityCommand.setModel(example);
        Proxy proxy = getProxy();
        proxy.dispatch(createEntityCommand, Target.PERSONAL_INFO_CONTROLLER);
        Optional<PersonalInfo> optionalPersonalInfo = createEntityCommand.getOptionalResult();
        if (optionalPersonalInfo.isEmpty()){
            throw new ResourceCreationException(PersonalInfo.class.getName());
        }

        PersonalInfo addedPersonalInfo = optionalPersonalInfo.get();
        oldEntity.setPersonalInfo(addedPersonalInfo);
    }

    private boolean isPersonalInfoMissing(User oldEntity) {
        return oldEntity.getPersonalInfo() == null;
    }

    private void addSellerTo(User oldEntity, SellerDto sellerDto) {
        Seller example = SellerSynchronizer.exampleOf(sellerDto);
        SellerCommand entityCreateCommand = new SellerCommand(CommandType.CREATE);
        entityCreateCommand.setModel(example);
        Proxy proxy = getProxy();
        proxy.dispatch(entityCreateCommand, Target.SELLER_CONTROLLER);
        Optional<Seller> optionalCreatedSeller = entityCreateCommand.getOptionalResult();
        if (optionalCreatedSeller.isEmpty()){
            throw new ResourceCreationException(Seller.class.getName());
        }

        Seller addedSeller = optionalCreatedSeller.get();
        SellerDtoCommand updateDtoCommand = new SellerDtoCommand(CommandType.UPDATE);
        long id = addedSeller.getId();
        sellerDto.setId(id);
        updateDtoCommand.setId(id);
        updateDtoCommand.setModel(sellerDto);
        proxy.dispatch(updateDtoCommand, Target.SELLER_MAPPER);

        if (updateDtoCommand.getOptionalResult().isEmpty()){
            throw new ResourceUpdateException(SellerDto.class.getName(), id);
        }

        SellerCommand sellerReadCommand = new SellerCommand(CommandType.READ);
        Seller idExample = SellerSynchronizer.idExampleOf(sellerDto);
        sellerReadCommand.setModel(idExample);
        proxy.dispatch(sellerReadCommand, Target.SELLER_CONTROLLER);
        Optional<Seller> optionalReadSeller = sellerReadCommand.getOptionalResult();
        if (optionalReadSeller.isEmpty()){
            throw new ResourceNotFoundException(Seller.class.getName(), id);
        }

        Seller readSeller = optionalReadSeller.get();
        oldEntity.setSeller(readSeller);
    }

    private boolean isSellerMissing(User oldEntity) {
        return oldEntity.getSeller() == null;
    }

    /**
     *Copy of addSellerTo
     */
    private void addCustomerTo(User oldEntity, CustomerDto customerDto) {
        Customer example = CustomerSynchronizer.exampleOf(customerDto);
        CustomerCommand entityCreateCommand = new CustomerCommand(CommandType.CREATE);
        entityCreateCommand.setModel(example);
        Proxy proxy = getProxy();
        proxy.dispatch(entityCreateCommand, Target.CUSTOMER_CONTROLLER);
        Optional<Customer> optionalCreatedCustomer = entityCreateCommand.getOptionalResult();
        if (optionalCreatedCustomer.isEmpty()){
            throw new ResourceCreationException(Customer.class.getName());
        }

        Customer addedCustomer = optionalCreatedCustomer.get();
        ControllerCommand<CustomerDto> updateDtoCommand = new ControllerCommand<>(CommandType.UPDATE);
        long id = addedCustomer.getId();
        customerDto.setId(id);
        updateDtoCommand.setId(id);
        updateDtoCommand.setModel(customerDto);
        proxy.dispatch(updateDtoCommand, Target.CUSTOMER_MAPPER);

        if (updateDtoCommand.getOptionalResult().isEmpty()){
            throw new ResourceUpdateException(CustomerDto.class.getName(), id);
        }

        CustomerCommand customerReadCommand = new CustomerCommand(CommandType.READ);
        Customer idExample = CustomerSynchronizer.idExampleOf(customerDto);
        customerReadCommand.setModel(idExample);
        proxy.dispatch(customerReadCommand, Target.CUSTOMER_CONTROLLER);
        Optional<Customer> optionalReadCustomer = customerReadCommand.getOptionalResult();
        if (optionalReadCustomer.isEmpty()){
            throw new ResourceNotFoundException(Customer.class.getName(), id);
        }

        Customer readCustomer = optionalReadCustomer.get();
        oldEntity.setCustomer(readCustomer);
    }

    private boolean isCustomerMissing(User oldEntity) {
        return oldEntity.getCustomer() == null;
    }
}
