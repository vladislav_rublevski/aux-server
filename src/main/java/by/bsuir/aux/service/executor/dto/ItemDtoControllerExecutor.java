package by.bsuir.aux.service.executor.dto;

import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.ItemDto;
import by.bsuir.aux.service.logic.merge.ItemMerger;

import java.util.function.BiFunction;

public class ItemDtoControllerExecutor extends AbstractDtoControllerExecutor<ItemDto> {
    @Override
    Target getTargetMapper() {
        return Target.ITEM_MAPPER;
    }

    @Override
    String getResourceName() {
        return ItemDto.class.getName();
    }

    @Override
    BiFunction<ItemDto, ItemDto, ItemDto> getMerger() {
        return ItemMerger::merge;
    }
}
