package by.bsuir.aux.service.executor.entity;

import by.bsuir.aux.repository.entity.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public class ItemControllerExecutor extends AbstractControllerExecutor<Item> {

    @Autowired
    public ItemControllerExecutor(JpaRepository<Item, Long> repository) {
        super(repository);
    }
}
