package by.bsuir.aux.service.executor.dto;

import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.UserDto;
import by.bsuir.aux.service.logic.merge.UserMerger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.function.BiFunction;

@Component
@Qualifier("userDtoController")
public class UserDtoControllerExecutor extends AbstractDtoControllerExecutor<UserDto> {


    @Override
    Target getTargetMapper() {
        return Target.USER_MAPPER;
    }

    @Override
    String getResourceName() {
        return UserDto.class.getName();
    }

    @Override
    BiFunction<UserDto, UserDto, UserDto> getMerger() {
        return UserMerger::merge;
    }
}
