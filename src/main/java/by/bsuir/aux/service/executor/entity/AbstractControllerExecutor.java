package by.bsuir.aux.service.executor.entity;

import by.bsuir.aux.controller.command.Command;
import by.bsuir.aux.controller.command.CommandType;
import by.bsuir.aux.controller.command.ControllerCommand;
import by.bsuir.aux.controller.proxy.ProxyExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.function.BiConsumer;

public abstract class AbstractControllerExecutor<T> extends ProxyExecutor<T> {

    private JpaRepository<T, Long> repository;

    public AbstractControllerExecutor(JpaRepository<T, Long> repository) {
        this.repository = repository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void run(Command<T> command) {
        ControllerCommand<T> entityCommand = (ControllerCommand<T>) command;
        CommandType commandType = entityCommand.getCommandType();

        switch (commandType){
            case READ:
                runReadCommand(entityCommand);
                break;
            case DELETE:
                runDeleteCommand(entityCommand);
                break;
            case CREATE:
                runCreateCommand(entityCommand);
                break;
            case UPDATE:
                runUpdateCommand(entityCommand);
                break;
        }
    }


    private void runUpdateCommand(ControllerCommand<T> entityCommand) {
        /*
          Updates will be flushed to db when the transaction ends
         */
    }



    private void runCreateCommand(ControllerCommand<T> entityCommand) {
        T entity = entityCommand.getModel();
        T saved = repository.save(entity);
        entityCommand.setOptionalResult(Optional.of(saved));
    }

    private void runDeleteCommand(ControllerCommand<T> entityCommand) {
        long id = entityCommand.getId();
        repository.deleteById(id);
    }

    private void runReadCommand(ControllerCommand<T> entityCommand) {
        long id = entityCommand.getId();
        Optional<T> optionalItem = repository.findById(id);
        entityCommand.setOptionalResult(optionalItem);
    }
}
