package by.bsuir.aux.service.executor.mapper;

import by.bsuir.aux.controller.command.CommandType;
import by.bsuir.aux.controller.command.ItemCommand;
import by.bsuir.aux.controller.proxy.Proxy;
import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.AuctionDto;
import by.bsuir.aux.repository.dto.ItemDto;
import by.bsuir.aux.repository.entity.Auction;
import by.bsuir.aux.repository.entity.Item;
import by.bsuir.aux.repository.mapper.AuctionMapper;
import by.bsuir.aux.service.exception.ResourceCreationException;
import by.bsuir.aux.service.logic.merge.AuctionMerger;
import by.bsuir.aux.service.logic.sync.AuctionSynchronizer;
import by.bsuir.aux.service.logic.sync.ItemSynchronizer;

import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

public class AuctionMapperExecutor extends AbstractMapperExecutor<Auction, AuctionDto> {

    private AuctionMapper auctionMapper;

    public AuctionMapperExecutor() {
        this.auctionMapper = new AuctionMapper();
    }

    @Override
    Target getEntityControllerTarget() {
        return Target.AUCTION_CONTROLLER;
    }

    @Override
    AuctionDto mapToDto(Auction entity) {
        return auctionMapper.mapToDto(entity);
    }

    @Override
    Auction buildExampleFromDto(AuctionDto dto) {
        return AuctionSynchronizer.exampleOf(dto);
    }

    @Override
    String getEntityName() {
        return Auction.class.getName();
    }

    @Override
    void merge(Auction oldEntity, AuctionDto newDto) {
        Set<ItemDto> missingNodes = getMissingNodesOf(oldEntity, newDto);
        addMissingNodesTo(oldEntity, missingNodes);
        AuctionMerger.merge(oldEntity, newDto);
    }

    @Override
    Auction buildIdExampleFrom(AuctionDto newDto, long id) {
        newDto.setId(id);
        return AuctionSynchronizer.idExampleOf(newDto);
    }

    private void addMissingNodesTo(Auction oldEntity, Set<ItemDto> missingNodes) {
        for (ItemDto dto : missingNodes){
            Optional<Item> optionalFoundItem = findByDtoExample(dto);
            if (optionalFoundItem.isPresent()){
                Item foundItem = optionalFoundItem.get();
                oldEntity.addItem(foundItem);
                continue;
            }
            /*
             * Item is leaf
             */
            Optional<Item> optionalAddedItem = addMissingItemFrom(dto);
            if (optionalAddedItem.isPresent()){
                Item addedItem = optionalAddedItem.get();
                oldEntity.addItem(addedItem);
            }else{
                throw new ResourceCreationException(Item.class.getName());
            }
        }
    }

    private Optional<Item> addMissingItemFrom(ItemDto dto) {
        ItemCommand entityCreateCommand = new ItemCommand(CommandType.CREATE);
        Item example = ItemSynchronizer.exampleOf(dto);
        entityCreateCommand.setModel(example);
        Proxy proxy = getProxy();
        proxy.dispatch(entityCreateCommand, Target.ITEM_CONTROLLER);
        return entityCreateCommand.getOptionalResult();
    }

    private Optional<Item> findByDtoExample(ItemDto dto) {
        Item example = ItemSynchronizer.exampleOf(dto);
        ItemCommand entityReadCommand = new ItemCommand(CommandType.READ);
        entityReadCommand.setModel(example);
        Proxy proxy = getProxy();
        proxy.dispatch(entityReadCommand, Target.ITEM_CONTROLLER);
        Optional<List<Item>> optionalEntities = entityReadCommand.getOptionalResults();
        if (optionalEntities.isPresent()){
            Item fst = optionalEntities.get().get(0);
            return Optional.of(fst);
        }else {
            return Optional.empty();
        }
    }

    private Set<ItemDto> getMissingNodesOf(Auction oldEntity, AuctionDto newDto) {
        Queue<ItemDto> itemQueue = newDto.getItemDtoQueue();
        Set<Item> items = oldEntity.getItems();
        return itemQueue.parallelStream()
                .filter(dto -> items.parallelStream().anyMatch(entity -> ItemSynchronizer.isSame(entity, dto)))
                .collect(Collectors.toUnmodifiableSet());
    }
}
