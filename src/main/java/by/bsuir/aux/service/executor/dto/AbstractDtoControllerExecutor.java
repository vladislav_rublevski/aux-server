package by.bsuir.aux.service.executor.dto;

import by.bsuir.aux.controller.command.Command;
import by.bsuir.aux.controller.command.CommandType;
import by.bsuir.aux.controller.command.ControllerCommand;
import by.bsuir.aux.controller.proxy.Proxy;
import by.bsuir.aux.controller.proxy.ProxyExecutor;
import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.service.exception.ResourceNotFoundException;

import java.util.Optional;
import java.util.function.BiFunction;

public abstract class AbstractDtoControllerExecutor<T> extends ProxyExecutor<T> {

    abstract Target getTargetMapper();
    abstract String getResourceName();
    abstract BiFunction<T, T, T> getMerger();

    @Override
    public void run(Command<T> command) {
        ControllerCommand<T> controllerCommand = (ControllerCommand<T>) command;
        CommandType commandType = controllerCommand.getCommandType();
        switch (commandType){
            case DELETE:
            case CREATE:
            case READ:
                dispatchToMapper(controllerCommand);
                break;
            case UPDATE:
                runUpdateCommand(controllerCommand);
                break;
        }
    }

    private void runUpdateCommand(ControllerCommand<T> controllerCommand) {
        long id = controllerCommand.getId();
        ControllerCommand<T> dtoReadCommand = new ControllerCommand<>(CommandType.READ);
        dtoReadCommand.setId(id);
        Optional<T> optionalAuctionDto = findModelById(dtoReadCommand, getTargetMapper());
            if (optionalAuctionDto.isEmpty()){
            throw new ResourceNotFoundException(getResourceName(), id);
        }
        T oldDto = optionalAuctionDto.get();
        T newDto = controllerCommand.getModel();
        BiFunction<T, T, T> merger = getMerger();
        T merged = merger.apply(oldDto, newDto);
        controllerCommand.setModel(merged);
        Proxy proxy = getProxy();
        proxy.dispatch(controllerCommand, getTargetMapper());
    }

    private <T> Optional<T> findModelById(Command<T> command, Target target) {
        Proxy proxy = getProxy();
        proxy.dispatch(command, target);
        return command.getOptionalResult();
    }

    private void dispatchToMapper(ControllerCommand<T> controllerCommand) {
        Target target = getTargetMapper();
        Proxy proxy = getProxy();
        proxy.dispatch(controllerCommand, target);
    }
}
