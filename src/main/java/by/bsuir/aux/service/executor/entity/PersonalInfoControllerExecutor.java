package by.bsuir.aux.service.executor.entity;

import by.bsuir.aux.repository.entity.PersonalInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public class PersonalInfoControllerExecutor extends AbstractControllerExecutor<PersonalInfo> {

    @Autowired
    public PersonalInfoControllerExecutor(JpaRepository<PersonalInfo, Long> repository) {
        super(repository);
    }

}
