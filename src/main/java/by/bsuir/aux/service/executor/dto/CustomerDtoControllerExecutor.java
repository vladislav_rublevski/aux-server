package by.bsuir.aux.service.executor.dto;

import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.CustomerDto;

import java.util.function.BiFunction;

public class CustomerDtoControllerExecutor extends AbstractDtoControllerExecutor<CustomerDto> {
    @Override
    Target getTargetMapper() {
        return Target.CUSTOMER_MAPPER;
    }

    @Override
    String getResourceName() {
        return CustomerDto.class.getName();
    }

    @Override
    BiFunction<CustomerDto, CustomerDto, CustomerDto> getMerger() {

    }
}
