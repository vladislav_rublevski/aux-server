package by.bsuir.aux.service.executor.entity;

import by.bsuir.aux.repository.entity.Auction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public class AuctionControllerExecutor extends AbstractControllerExecutor {

    @Autowired
    public AuctionControllerExecutor(JpaRepository<Auction, Long> repository) {
        super(repository);
    }

}
