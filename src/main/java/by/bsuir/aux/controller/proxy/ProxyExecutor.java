package by.bsuir.aux.controller.proxy;

import by.bsuir.aux.controller.command.Command;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class ProxyExecutor<T>{
    private Proxy proxy;

    public Proxy getProxy() {
        return proxy;
    }

    @Autowired
    public void setProxy(Proxy proxy) {
        this.proxy = proxy;
    }

    public  abstract void run(Command<T> command);
}
