package by.bsuir.aux.controller.proxy;

import by.bsuir.aux.controller.command.Command;
import by.bsuir.aux.repository.dto.UserDto;
import by.bsuir.aux.repository.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

@Component
public class Proxy {
    private Map<Target, Object> targets;

    private ProxyExecutor<User> userControllerExecutor;

    private ProxyExecutor<UserDto> userMapperExecutor;
    private ProxyExecutor<UserDto> userDtoControllerExecutor;

    @Autowired
    public void setUserExecutor(ProxyExecutor<User> userProxyExecutor) {
        this.userControllerExecutor = userProxyExecutor;
    }

    @Autowired
    @Qualifier("userMapper")
    public void setUserMapperExecutor(ProxyExecutor<UserDto> userMapperExecutor) {
        this.userMapperExecutor = userMapperExecutor;
    }

    @Autowired
    @Qualifier("userDtoController")
    public void setUserDtoControllerExecutor(ProxyExecutor<UserDto> userDtoControllerExecutor) {
        this.userDtoControllerExecutor = userDtoControllerExecutor;
    }

    @PostConstruct
    private void initExecutors(){
        targets = Map.of(
                Target.USER_CONTROLLER, userControllerExecutor,
                Target.USER_DTO_CONTROLLER, userDtoControllerExecutor,
                Target.USER_MAPPER, userMapperExecutor
        );
    }

    public <T> void dispatch(Command<T> command, Target target){
        Object foundObjectTarget = targets.get(target);
        ProxyExecutor<T> executor = (ProxyExecutor<T>) foundObjectTarget;
        executor.run(command);
    }
}
