package by.bsuir.aux.controller.command.websocket.response;

import java.util.List;

public class Response<T>{
    private Status status;
    private List<T> listDto;

    Response() {
    }

    public Response(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public List<T> getListDto() {
        return listDto;
    }

    public void setListDto(List<T> listDto) {
        this.listDto = listDto;
    }
}
