package by.bsuir.aux.controller.command;

import by.bsuir.aux.repository.entity.Item;

public class ItemCommand extends Command<Item> {
    private long id;

    public ItemCommand(CommandType commandType) {
        super(commandType);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
