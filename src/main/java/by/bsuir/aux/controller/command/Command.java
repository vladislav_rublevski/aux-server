package by.bsuir.aux.controller.command;

import java.util.List;
import java.util.Optional;

public class Command<T> {
    private CommandType commandType;
    private T model;

    private Optional<T> optionalResult;
    private Optional<List<T>> optionalResults;

    public Command(CommandType commandType) {
        this.commandType = commandType;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }

    public Optional<T> getOptionalResult() {
        return optionalResult;
    }

    public void setOptionalResult(Optional<T> optionalResult) {
        this.optionalResult = optionalResult;
    }

    public Optional<List<T>> getOptionalResults() {
        return optionalResults;
    }

    public void setOptionalResults(Optional<List<T>> optionalResults) {
        this.optionalResults = optionalResults;
    }
}
