package by.bsuir.aux.controller.command;

import by.bsuir.aux.repository.dto.SellerDto;

public class SellerDtoCommand extends Command<SellerDto> {
    private long id;

    public SellerDtoCommand(CommandType commandType) {
        super(commandType);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
