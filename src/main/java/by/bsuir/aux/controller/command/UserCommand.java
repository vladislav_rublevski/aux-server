package by.bsuir.aux.controller.command;

import by.bsuir.aux.repository.entity.User;

public class UserCommand extends Command<User>{
    private long id;

    public UserCommand(CommandType commandType) {
        super(commandType);
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
