package by.bsuir.aux.controller.command;

import by.bsuir.aux.repository.dto.UserDto;

public class UserDtoCommand extends Command<UserDto> {
    private long id;

    public UserDtoCommand(CommandType commandType) {
        super(commandType);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
