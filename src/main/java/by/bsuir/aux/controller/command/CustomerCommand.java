package by.bsuir.aux.controller.command;

import by.bsuir.aux.repository.entity.Customer;

public class CustomerCommand extends Command<Customer> {
    private long id;

    public CustomerCommand(CommandType commandType) {
        super(commandType);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
