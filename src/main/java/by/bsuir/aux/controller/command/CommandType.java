package by.bsuir.aux.controller.command;

/**
 * Specifies command which should be performed on multiple resources
 * See AppController
 */
public enum CommandType {
    CREATE, READ, UPDATE, DELETE
}
