package by.bsuir.aux.controller.command;

import by.bsuir.aux.repository.entity.Seller;

public class SellerCommand extends Command<Seller> {
    private long id;

    public SellerCommand(CommandType commandType) {
        super(commandType);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
