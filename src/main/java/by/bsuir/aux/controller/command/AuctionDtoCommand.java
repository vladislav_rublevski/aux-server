package by.bsuir.aux.controller.command;

import by.bsuir.aux.repository.dto.AuctionDto;

public class AuctionDtoCommand extends Command<AuctionDto> {
    private long id;

    public AuctionDtoCommand(CommandType commandType) {
        super(commandType);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
