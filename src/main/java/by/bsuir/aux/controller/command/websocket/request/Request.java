package by.bsuir.aux.controller.command.websocket.request;

import by.bsuir.aux.controller.command.CommandType;

public class Request<T> {

    private T dto;

    private CommandType commandType;

    Request() {
    }

    public Request(CommandType commandType) {
        this.commandType = commandType;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    public T getDto() {
        return dto;
    }

    public void setDto(T dto) {
        this.dto = dto;
    }
}
