package by.bsuir.aux.controller.command;

public class ControllerCommand<T> extends Command<T> {
    private long id;

    public ControllerCommand(CommandType commandType) {
        super(commandType);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
