package by.bsuir.aux.controller.command;

import by.bsuir.aux.repository.dto.ItemDto;

public class ItemDtoCommand extends Command<ItemDto> {
    private long id;

    public ItemDtoCommand(CommandType commandType) {
        super(commandType);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
