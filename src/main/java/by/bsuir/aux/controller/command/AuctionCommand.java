package by.bsuir.aux.controller.command;

import by.bsuir.aux.repository.entity.Auction;

public class AuctionCommand extends Command<Auction> {
    private long id;

    public AuctionCommand(CommandType commandType) {
        super(commandType);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
