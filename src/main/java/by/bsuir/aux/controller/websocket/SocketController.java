package by.bsuir.aux.controller.websocket;

import by.bsuir.aux.controller.command.websocket.request.Request;
import by.bsuir.aux.controller.command.websocket.response.Response;
import by.bsuir.aux.controller.command.websocket.response.Status;
import by.bsuir.aux.repository.dto.AuctionDto;
import by.bsuir.aux.repository.dto.CustomerDto;
import by.bsuir.aux.repository.dto.SellerDto;
import by.bsuir.aux.repository.dto.UserDto;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.util.List;
import java.util.Set;

@Controller
public class SocketController {

    @MessageMapping("/auctions")
    @SendToUser("/queue/auctions")
    public Response<AuctionDto> auctionEndPoint(Principal principal, Request<AuctionDto> request){
        AuctionDto auctionDto = request.getDto();
        System.out.println(auctionDto.getSellerId());
        Response<AuctionDto> response = new Response<>(Status.OK);
        response.setListDto(List.of(auctionDto));
        return response;
    }

    @MessageMapping("/users")
    @SendToUser("/queue/users")
    public Response<UserDto> userEndPoint(Principal principal, Request<Void> request){
        AuctionDto auctionDto = new AuctionDto();
        auctionDto.setSellerId(1);
        Set<AuctionDto> auctionDtos = Set.of(auctionDto);
        SellerDto sellerDto = new SellerDto();
        sellerDto.setAuctions(auctionDtos);
        CustomerDto customerDto = new CustomerDto();
        customerDto.setAuctions(auctionDtos);
        UserDto userDto = new UserDto(1);
        userDto.setCustomer(customerDto);
        userDto.setSeller(sellerDto);
        Response<UserDto> response = new Response<>(Status.OK);
        response.setListDto(List.of(userDto));
        return response;
    }
}
