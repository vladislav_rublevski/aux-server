package by.bsuir.aux.controller.web;

import by.bsuir.aux.controller.command.CommandType;
import by.bsuir.aux.controller.command.UserDtoCommand;
import by.bsuir.aux.controller.proxy.Proxy;
import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

@org.springframework.web.bind.annotation.RestController
@RequestMapping(path="/users")
public class UserController {

    private Proxy proxy;

    @Autowired
    void setProxy(Proxy proxy) {
        this.proxy = proxy;
    }

    @GetMapping(value = "/{userId}", produces = "application/json")
    public @ResponseBody
    UserDto getAuthor(@PathVariable long userId){
        System.out.println(userId);
        UserDtoCommand userDtoCommand = new UserDtoCommand(CommandType.READ);
        userDtoCommand.setId(userId);
        proxy.dispatch(userDtoCommand, Target.USER_DTO_CONTROLLER);
        Optional<UserDto> optionalUserDto = userDtoCommand.getOptionalResult();
        return optionalUserDto.orElse(null);
    }
}
