package by.bsuir.aux.controller.config.web;

import by.bsuir.aux.controller.config.datasource.DataSourceConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@Import(DataSourceConfig.class)
@ComponentScan(basePackages = {
        "by.bsuir.aux.service.executor",
        "by.bsuir.aux.controller.proxy"
})
@EnableTransactionManagement
public class RootConfig {
}
