package by.bsuir.aux.service.executor;

import by.bsuir.aux.controller.command.CommandType;
import by.bsuir.aux.controller.command.UserDtoCommand;
import by.bsuir.aux.controller.proxy.Proxy;
import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.service.executor.dto.UserDtoControllerExecutor;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserControllerConfiguration.class)
public class UserDtoControllerExecutorTest {
    private UserDtoControllerExecutor userDtoController;
    private Proxy proxy;

    @Autowired
    public void setUserDtoController(UserDtoControllerExecutor userDtoController) {
        this.userDtoController = userDtoController;
    }

    @Autowired
    public void setProxy(Proxy proxy) {
        this.proxy = proxy;
    }

    @Test
    public void getCommandTest(){
        UserDtoCommand userDtoCommand = new UserDtoCommand(CommandType.READ);
        userDtoCommand.setId(1);
        proxy.dispatch(userDtoCommand, Target.USER_DTO_CONTROLLER);
        boolean valid = userDtoCommand.getOptionalResult().isPresent();
        Assert.assertTrue(valid);
    }
}