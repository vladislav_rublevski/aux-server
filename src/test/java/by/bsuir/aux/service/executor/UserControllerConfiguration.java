package by.bsuir.aux.service.executor;

import by.bsuir.aux.IntegrationConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@Import(IntegrationConfiguration.class)
@ComponentScan(basePackages = {
        "by.bsuir.aux.service.executor",
        "by.bsuir.aux.controller.proxy"
})
@EnableTransactionManagement
public class UserControllerConfiguration {
}
