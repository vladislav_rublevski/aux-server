package by.bsuir.aux.service.executor;

import by.bsuir.aux.controller.command.CommandType;
import by.bsuir.aux.controller.command.UserCommand;
import by.bsuir.aux.controller.proxy.Proxy;
import by.bsuir.aux.controller.proxy.Target;
import by.bsuir.aux.repository.entity.User;
import by.bsuir.aux.service.executor.entity.UserControllerExecutor;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserControllerConfiguration.class)
@Transactional
public class UserControllerExecutorTest {
    private UserControllerExecutor userController;
    private Proxy proxy;

    @Autowired
    public void setUserController(UserControllerExecutor userController) {
        this.userController = userController;
    }

    @Autowired
    public void setProxy(Proxy proxy) {
        this.proxy = proxy;
    }

    @Test
    public void getCommandTest(){
        UserCommand getCommand = new UserCommand(CommandType.READ);
        getCommand.setId(1);
        proxy.dispatch(getCommand, Target.USER_CONTROLLER);
        Optional<User> optionalUser = getCommand.getOptionalResult();
        boolean valid = optionalUser.isPresent() && optionalUser.get().getId() == 1 &&
                optionalUser.get().getSeller().getId() == 1 &&
                optionalUser.get().getCustomer().getId() == 1 &&
                optionalUser.get().getSeller().getAuctions().size() == 1;
        Assert.assertTrue(valid);
    }
}
