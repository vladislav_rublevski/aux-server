package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.UserDto;
import by.bsuir.aux.repository.entity.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDate;


public class UserMapperTest {

    @Test
    public void mapToDto(){
        Auction auction = new Auction();
        Seller seller = new Seller(1);
        seller.setRate(2);
        auction.setSeller(seller);
        Customer customer = new Customer(1);
        customer.setRate(2);
        auction.addCustomer(customer);
        auction.setBeginningDate(LocalDate.EPOCH);
        auction.setExpirationDate(LocalDate.EPOCH);
        Item item = new Item();
        item.setName("Book");
        auction.addItem(item);

        PersonalInfo personalInfo = new PersonalInfo();
        personalInfo.setName("Mike");
        User user = new User();

        user.setPersonalInfo(personalInfo);
        user.setCustomer(customer);
        user.setSeller(seller);

        user.setLogin("Sidius");
        user.setPassword("Star");

        UserMapper userMapper = new UserMapper();
        UserDto userDto = userMapper.mapToDto(user);

        boolean valid = userDto.getId() == user.getId() &&
                userDto.getLogin() == user.getLogin() &&
                userDto.getPassword() == user.getPassword() &&
                userDto.getPersonalInfo().getName() == user.getPersonalInfo().getName() &&
                userDto.getCustomer().getRate() == user.getCustomer().getRate() &&
                userDto.getSeller().getRate() == user.getSeller().getRate() &&
                userDto.getSeller().getAuctions().size() == 1;
        Assert.assertTrue(valid);
    }

    @Test
    public void mapToEntity(){

    }
}