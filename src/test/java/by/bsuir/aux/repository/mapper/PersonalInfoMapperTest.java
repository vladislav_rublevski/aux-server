package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.PersonalInfoDto;
import by.bsuir.aux.repository.entity.PersonalInfo;
import by.bsuir.aux.repository.entity.User;
import org.junit.Assert;
import org.testng.annotations.Test;


public class PersonalInfoMapperTest {

    @Test
    public void mapToDtoTest(){
        PersonalInfo personalInfo = new PersonalInfo(1);
        User user = new User();
        personalInfo.setUser(user);

        personalInfo.setName("Mike");

        PersonalInfoMapper personalInfoMapper = new PersonalInfoMapper();
        PersonalInfoDto personalInfoDto = personalInfoMapper.mapToDto(personalInfo);
        boolean valid = personalInfoDto.getName().equals("Mike");
        Assert.assertTrue(valid);
    }
}