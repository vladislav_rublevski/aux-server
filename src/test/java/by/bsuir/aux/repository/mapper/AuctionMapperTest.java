package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.AuctionDto;
import by.bsuir.aux.repository.dto.ItemDto;
import by.bsuir.aux.repository.entity.*;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;


public class AuctionMapperTest {

    private AuctionMapper mapper;

    @BeforeTest
    public void initMapper(){
        mapper = new AuctionMapper();
    }

    @Test
    public void mapToDtoTest() {
        Auction auction = new Auction();
        Seller seller = new Seller(1);
        auction.setSeller(seller);
        Customer customer = new Customer(1);
        auction.addCustomer(customer);
        auction.setBeginningDate(LocalDate.EPOCH);
        auction.setExpirationDate(LocalDate.EPOCH);
        Item item = new Item();
        item.setDescription("Description");
        item.setName("Book");
        item.setCurrentPrice(50);
        auction.addItem(item);

        AuctionDto auctionDto = mapper.mapToDto(auction);

        boolean valid = auctionDto.getId() == 0 && auctionDto.getSellerId() == seller.getId()
                && auctionDto.getBeginningDate().equals(auction.getBeginningDate())
                && auctionDto.getExpirationDate().equals(auction.getExpirationDate())
                && auctionDto.getItemDtoQueue().size() == 1
                && auctionDto.getCustomersIds().size() == 1;
        Assert.assertTrue(valid);
    }

    @Test
    public void mapToEntityTest(){
        AuctionDto auctionDto = new AuctionDto();
        auctionDto.setSellerId(1);
        auctionDto.setBeginningDate(LocalDate.EPOCH);
        auctionDto.setExpirationDate(LocalDate.EPOCH);

        ItemDto itemDto = new ItemDto();
        itemDto.setRate(1);
        itemDto.setCurrentPrice(100);
        itemDto.setDescription("Description");
        itemDto.setName("Book");
        Queue<ItemDto> itemDtoQueue = new ArrayDeque<>();
        itemDtoQueue.add(itemDto);
        auctionDto.setItemDtoQueue(itemDtoQueue);

        Seller seller = new Seller();
        Set<Customer> customers = new HashSet<>();
        Auction auction = mapper.mapToEntity(auctionDto, seller, customers);
        boolean valid = auction.getSeller().getId() == 0
                && auction.getExpirationDate() == LocalDate.EPOCH
                && auction.getBeginningDate() == LocalDate.EPOCH
                && auction.getItems().size() == 1
                && auction.getCustomers().size() == 0;

        Assert.assertTrue(valid);
    }
}