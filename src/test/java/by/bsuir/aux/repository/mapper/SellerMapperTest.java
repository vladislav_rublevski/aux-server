package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.AuctionDto;
import by.bsuir.aux.repository.dto.SellerDto;
import by.bsuir.aux.repository.entity.*;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.Set;

public class SellerMapperTest {

    private SellerMapper sellerMapper;

    @BeforeTest
    public void initMapper(){
        sellerMapper = new SellerMapper();
    }

    @Test
    public void mapToDtoTest() {
        Auction auction = new Auction();
        Seller seller = new Seller(1);
        seller.setRate(2);
        auction.setSeller(seller);
        Customer customer = new Customer(1);
        customer.setRate(2);
        auction.addCustomer(customer);
        auction.setBeginningDate(LocalDate.EPOCH);
        auction.setExpirationDate(LocalDate.EPOCH);
        Item item = new Item();
        item.setDescription("Description");
        item.setName("Book");
        item.setCurrentPrice(50);
        auction.addItem(item);
        User user = new User(1);
        seller.setUser(user);

        SellerDto sellerDto = sellerMapper.mapToDto(seller);

        boolean valid = sellerDto.getId() == seller.getId()
                && sellerDto.getRate() == seller.getRate()
                && sellerDto.getAuctions().size() == 1
                && sellerDto.getAuctions().stream().anyMatch(a -> a.getFirstItem().getName().equals("Book"))
                && sellerDto.getUserId() == 1;
        Assert.assertTrue(valid);
    }

    @Test
    public void mapToEntity(){
        SellerDto sellerDto = new SellerDto();
        sellerDto.setId(1);
        sellerDto.setRate(50);
        sellerDto.setUserId(1);
        AuctionDto auctionDto = new AuctionDto();
        sellerDto.setAuctions(Set.of(auctionDto));
        User user = new User(1);
        Auction auction = new Auction();

        Seller seller = sellerMapper.mapToEntity(sellerDto, user, Set.of(auction));

        boolean valid = seller.getId() == 1
                && seller.getRate() == 50
                && seller.getUser().getId() == 1
                && seller.getAuctions().size() == 1;
        Assert.assertTrue(valid);
    }
}