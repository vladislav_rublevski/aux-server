package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.ItemDto;
import by.bsuir.aux.repository.entity.Item;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ItemMapperTest {
    private ItemMapper itemMapper;

    @BeforeTest
    public void initMapper(){
        itemMapper = new ItemMapper();
    }

    @Test
    public void mapToDtoTest(){
        Item item = new Item();
        item.setDescription("Description");
        item.setName("Book");
        item.setCurrentPrice(50);
        item.setRate(10);

        ItemDto itemDto = itemMapper.mapToDto(item);

        boolean valid = itemDto.getDescription().equals("Description")
                && itemDto.getCurrentPrice() == 50
                && itemDto.getName().equals("Book")
                && itemDto.getRate() == 10;
        Assert.assertTrue(valid);
    }

    @Test
    public void mapToEntityTest(){
        ItemDto itemDto = new ItemDto();
        itemDto.setName("Book");
        itemDto.setDescription("Description");
        itemDto.setCurrentPrice(100);
        itemDto.setRate(50);

        Item item = itemMapper.mapToEntity(itemDto);

        boolean valid = item.getName().equals("Book")
                && item.getDescription().equals("Description")
                && item.getCurrentPrice() == 100
                && item.getRate() == 50;

        Assert.assertTrue(valid);
    }
}