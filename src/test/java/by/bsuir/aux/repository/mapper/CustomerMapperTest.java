package by.bsuir.aux.repository.mapper;

import by.bsuir.aux.repository.dto.AuctionDto;
import by.bsuir.aux.repository.dto.CustomerDto;
import by.bsuir.aux.repository.entity.*;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.Set;

public class CustomerMapperTest {

    private CustomerMapper customerMapper;

    @BeforeTest
    public void initMapper(){
        customerMapper = new CustomerMapper();
    }

    @Test
    public void mapToDtoTest(){
        Auction auction = new Auction();
        Seller seller = new Seller(1);
        auction.setSeller(seller);
        Customer customer = new Customer(1);
        customer.setRate(2);
        auction.addCustomer(customer);
        auction.setBeginningDate(LocalDate.EPOCH);
        auction.setExpirationDate(LocalDate.EPOCH);
        Item item = new Item();
        item.setDescription("Description");
        item.setName("Book");
        item.setCurrentPrice(50);
        auction.addItem(item);
        User user = new User(1);
        customer.setUser(user);

        CustomerDto customerDto = customerMapper.mapToDto(customer);
        boolean valid = customerDto.getId() == customer.getId()
                && customerDto.getRate() == customer.getRate()
                && customerDto.getAuctions().size() == 1
                && customerDto.getAuctions().stream().anyMatch(a -> a.getFirstItem().getName().equals("Book"));
        Assert.assertTrue(valid);
    }

    @Test
    public void mapToEntityTest(){
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(1);
        customerDto.setRate(5);
        AuctionDto auctionDto = new AuctionDto();
        auctionDto.setId(1);
        customerDto.setAuctions(Set.of(auctionDto));

        User user = new User(1);
        Auction auction = new Auction();
        Customer customer = customerMapper.mapToEntity(customerDto, user, Set.of(auction));

        boolean valid = customer.getId() == 1
                && customer.getRate() == 5
                && customer.getAuctions().size() == 1
                && customer.getUser().getId() == 1;
        Assert.assertTrue(valid);
    }
}