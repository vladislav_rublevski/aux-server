package by.bsuir.aux.controller;

import by.bsuir.aux.controller.command.CommandType;
import by.bsuir.aux.controller.command.websocket.request.Request;
import by.bsuir.aux.controller.command.websocket.response.Response;
import by.bsuir.aux.repository.dto.AuctionDto;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class GreetingIntegrationTests {
	private int port = 8080;

	private SockJsClient sockJsClient;

	private WebSocketStompClient stompClient;

	private final WebSocketHttpHeaders headers = new WebSocketHttpHeaders();

	@Before
	public void setup() {
		List<Transport> transports = new ArrayList<>();
		transports.add(new WebSocketTransport(new StandardWebSocketClient()));
		this.sockJsClient = new SockJsClient(transports);

		this.stompClient = new WebSocketStompClient(sockJsClient);
		this.stompClient.setMessageConverter(new MappingJackson2MessageConverter());
	}

	@Test
	public void getGreeting() throws Exception {
	    StompSessionHandler handler = new StompSessionHandlerAdapter() {

			@Override
			public void afterConnected(final StompSession session, StompHeaders connectedHeaders) {
				session.subscribe("/queue/responses", new StompFrameHandler() {
					@Override
					public Type getPayloadType(StompHeaders headers) {
						return Response.class;
					}

					@Override
					public void handleFrame(StompHeaders headers, Object payload) {
						Response<AuctionDto> response = (Response<AuctionDto>) payload;
						try {
							assertEquals(1L, response.getListDto().get(0).getSellerId());
						} finally {
							session.disconnect();
						}
					}
				});
				session.send("/auctions", new Request(CommandType.CREATE));
			}
		};

		this.stompClient.connect("ws://localhost:{port}/marcopolo", this.headers, handler, this.port);

	}
}
