package by.bsuir.aux.controller;

import by.bsuir.aux.IntegrationConfiguration;
import by.bsuir.aux.repository.entity.User;
import by.bsuir.aux.repository.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = IntegrationConfiguration.class)
@Transactional
public class IntegrationTest {

    @Autowired
    private UserRepository repository;

    @Test
    public void hikariConnectionPoolIsConfigured() {
        Iterable<User> optionalUser = repository.findAll();
        User user = optionalUser.iterator().next();
        System.out.println(user.getLogin());
    }
}
