package by.bsuir.aux;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

@Configuration
@EnableJpaRepositories(basePackages = "by.bsuir.aux.repository.repository")
public class IntegrationConfiguration {
    private static final String PROPERTIES_FILE = "application.properties";
    private static final String URL = "url";
    private static final String DRIVER_NAME = "driver_name";
    private static final String USER_NAME = "user_name";
    private static final String PASSWORD = "password";

    @Bean
    public DataSource dataSource() throws IOException {
        InputStream inputStream = IntegrationConfiguration.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE);

        Properties properties = new Properties();
        properties.load(Objects.requireNonNull(inputStream));

        String url = properties.getProperty(URL, "null");
        String driverName = properties.getProperty(DRIVER_NAME, "null");
        String userName = properties.getProperty(USER_NAME, "null");
        String password = properties.getProperty(PASSWORD, "null");

        inputStream.close();

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(driverName);
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(userName);
        hikariConfig.setPassword(password);

        return new HikariDataSource(hikariConfig);
    }
    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) throws IOException {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws IOException {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("by.bsuir.aux.repository.entity");
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(hibernateProperties());

        return em;
    }

    private Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty(
                "hibernate.hbm2ddl.auto", "validate");
        hibernateProperties.setProperty(
                "hibernate.default_schema", "public");
        hibernateProperties.setProperty(
                "hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        hibernateProperties.setProperty("show_sql", "true");
        hibernateProperties.setProperty("format_sql", "true");
        hibernateProperties.setProperty("use_sql_comments", "true");

        return hibernateProperties;
    }
}
