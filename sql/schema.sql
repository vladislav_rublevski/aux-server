create schema public;

comment on schema public is 'standard public schema';

create table if not exists customer
(
	id bigserial not null
		constraint customer_pkey
			primary key,
	rate integer not null
);

create table if not exists personal_info
(
	id bigserial not null
		constraint personal_info_pkey
			primary key,
	name varchar(255)
);

create table if not exists seller
(
	id bigserial not null
		constraint seller_pkey
			primary key,
	rate integer not null
);

create table if not exists auction
(
	id bigserial not null
		constraint auction_pkey
			primary key,
	beginning_date date,
	expiration_date date,
	description varchar(255),
	init_cost integer,
	name varchar(255),
	rate integer,
	seller_id bigint
		constraint fkq5myjawt6s0m3nsi66wqld3p8
			references seller
);

create table if not exists customer_auction
(
	cust_id bigint not null
		constraint fkm0nvxc4ka8tc1fo2y0fndjhp5
			references customer,
	aux_id bigint not null
		constraint fk58trbu3fcmams0sr8ldcmqyrj
			references auction,
	constraint customer_auction_pkey
		primary key (cust_id, aux_id)
);

create table if not exists "user"
(
	id bigserial not null
		constraint user_pkey
			primary key,
	login varchar(255),
	password varchar(255),
	customer_id bigint
		constraint fkdptx0i3ky01svofwjytq5iry0
			references customer,
	personal_info_id bigint
		constraint fk1vnmpl9hxv1j8ure47h97lhco
			references personal_info,
	seller_id bigint
		constraint fkcpth9w5maddfjn080jhva9sx2
			references seller
);
